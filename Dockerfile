FROM golang:latest AS builder
ARG GIT_TAG
RUN git clone https://git.torproject.org/pluggable-transports/snowflake.git
WORKDIR snowflake/proxy
RUN git checkout $GIT_TAG
RUN go build

FROM ubuntu:latest
WORKDIR /root/
COPY --from=builder /go/snowflake/proxy/proxy ./
CMD ["./proxy"]
