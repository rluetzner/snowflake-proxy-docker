#!/bin/bash
set -o errexit
set -o pipefail
set -o nounset

git pull

readarray -t UPSTREAM_RELEASES < <(git ls-remote https://git.torproject.org/pluggable-transports/snowflake.git | awk '{print $2}' | grep 'tags/v' | sed 's|refs/tags/||' | sort --version-sort)
DOCKER_TAGS=$(git tag)

build_push_image() {
    local RELEASE=$1
    echo "New release to build: $RELEASE"
    docker build --build-arg GIT_TAG=$RELEASE -t rluetzner/snowflake-proxy:$RELEASE .
    docker push rluetzner/snowflake-proxy:$RELEASE
    git tag $RELEASE
}

docker_image_hash_for_tag() {
    local TAG=$1
    docker images rluetzner/snowflake-proxy | grep $TAG | awk '{print $3}'
}

for RELEASE in ${UPSTREAM_RELEASES[@]}
do
    echo $DOCKER_TAGS | grep --word-regexp --quiet $RELEASE || \
        build_push_image $RELEASE
done

LATEST=${UPSTREAM_RELEASES[-1]}
echo "Latest: $LATEST"
CURRENT_LATEST_HASH=$(docker_image_hash_for_tag "latest")
TARGET_LATEST_HASH=$(docker_image_hash_for_tag "$LATEST")

if [ $CURRENT_LATEST_HASH != $TARGET_LATEST_HASH ]
then
    echo "Updating latest tag to $TARGET_LATEST_HASH"
    docker tag "rluetzner/snowflake-proxy:$LATEST" rluetzner/snowflake-proxy:latest
    docker push rluetzner/snowflake-proxy:latest
else
    echo "Current Tag is already correctly set."
fi

git push --tags
