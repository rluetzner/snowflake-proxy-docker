# Snowflake-Proxy-Docker

[![Docker Image Version (latest semver)](https://img.shields.io/docker/v/rluetzner/snowflake-proxy?sort=semver)](https://hub.docker.com/r/rluetzner/snowflake-proxy)
[![Docker Stars](https://img.shields.io/docker/stars/rluetzner/snowflake-proxy)](https://hub.docker.com/r/rluetzner/snowflake-proxy)

This repository contains a Dockerfile to build the [Tor Snowflake-Proxy](https://community.torproject.org/relay/setup/snowflake/standalone/) and a docker-compose file to run it.

Find the tagged images on [Docker Hub](https://hub.docker.com/r/rluetzner/snowflake-proxy).

## Run with docker-compose

```bash
git clone https://codeberg.org/rluetzner/snowflake-proxy-docker
cd snowflake-proxy-docker

# I'm using docker-compose 2.
# If you're using version 1, you must insert a dash between docker and compose.
docker compose up -d
```

## Building

```bash
# Get tags from the Snowflake repository
git ls-remote https://git.torproject.org/pluggable-transports/snowflake.git | \
    awk '{print $2}' | \
    grep 'tags/v' | \
    sed 's|refs/tags/||'

# You should probably replace my name if you're planning to publish another build yourself.
TAG=v2.1.0; docker build --build-arg GIT_TAG=$TAG -t rluetzner/snowflake-proxy:$TAG .
```

## Publishing

```bash
# This is only needed once.
docker login

docker push rluetzner/snowflake-proxy
```

## A poor man's CD

Currently I use cron on my Raspberry Pi to build and publish new releases.

```text
# Build Snowflake-Proxy Docker images
0 1 * * * cd snowflake && ./build-all-releases.sh
```
